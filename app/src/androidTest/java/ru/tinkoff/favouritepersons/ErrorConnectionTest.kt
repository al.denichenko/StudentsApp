package ru.tinkoff.favouritepersons

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.github.tomakehurst.wiremock.stubbing.Scenario
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import fileToString
import kotlinx.serialization.json.Json
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import ru.tinkoff.favouritepersons.data.Person
import ru.tinkoff.favouritepersons.pages.FavouritePersonsPage
import ru.tinkoff.favouritepersons.presentation.activities.MainActivity
import ru.tinkoff.favouritepersons.room.PersonDataBase
import ru.tinkoff.myupgradeapplication.week6.rules.LocalhostPreferenceRule

@RunWith(AndroidJUnit4::class)
class ErrorConnectionTest : TestCase() {
    @get:Rule
    val ruleChain: RuleChain = RuleChain.outerRule(LocalhostPreferenceRule())
        .around(WireMockRule(5000))
        .around(ActivityScenarioRule(MainActivity::class.java))

    private lateinit var db: PersonDataBase
    private val person = Json.decodeFromString<Person>(fileToString("mock/user_igor.json"))

    @Before
    fun createDb() {
        db = PersonDataBase.getDBClient(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @After
    fun clearDB() {
        db.personsDao().clearTable()
    }

    @Test
    fun errorConnectionTest() {
        WireMock.stubFor(
            WireMock.get(WireMock.urlEqualTo("/api/"))
                .willReturn(
                    aResponse()
                        .withBody(fileToString("mock/error.json"))
                )
        )

        run {
            FavouritePersonsPage {
                step("Check error connection") {
                    clickAddPersonButton()
                    clickAddPersonByNetworkButton()
                    Thread.sleep(1000)
                    checkErrorConnection()
                }
            }
        }
    }
}