package ru.tinkoff.favouritepersons

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.github.tomakehurst.wiremock.stubbing.Scenario
import fileToString
import junit.framework.TestCase
import kotlinx.serialization.json.Json
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith
import ru.tinkoff.favouritepersons.data.Person
import ru.tinkoff.favouritepersons.pages.EditPage
import ru.tinkoff.favouritepersons.pages.FavouritePersonsPage
import ru.tinkoff.favouritepersons.presentation.activities.MainActivity
import ru.tinkoff.favouritepersons.room.PersonDataBase
import ru.tinkoff.myupgradeapplication.week6.rules.LocalhostPreferenceRule

@RunWith(AndroidJUnit4::class)
class AddAndEditStudent : com.kaspersky.kaspresso.testcases.api.testcase.TestCase() {
    @get:Rule
    val ruleChain: RuleChain = RuleChain.outerRule(LocalhostPreferenceRule())
        .around(WireMockRule(5000))
        .around(ActivityScenarioRule(MainActivity::class.java))

    private lateinit var db: PersonDataBase
    private val person = Json.decodeFromString<Person>(fileToString("mock/user_igor.json"))

    @Before
    fun createDb() {
        db = PersonDataBase.getDBClient(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @After
    fun clearDB() {
        db.personsDao().clearTable()
    }

    @Test
    fun openPageWithStudentsData() {
        WireMock.stubFor(
            WireMock.get(WireMock.urlEqualTo("/api/"))
                .willReturn(
                    WireMock.ok(fileToString("mock/user_igor.json"))
                )
        )

        run {
            FavouritePersonsPage {
                step("Add Students") {
                    clickAddPersonButton()
                    clickAddPersonByNetworkButton()
                    Thread.sleep(1000)
                    clickPerson()
                }
            }
            EditPage {
                step("Check students data") {
                    checkName()
                    checkSurname()
                    checkGender()
                    checkBirthday()
                }
            }
        }
    }

    @Test
    fun checkEditStudent() {
        WireMock.stubFor(
            WireMock.get(WireMock.urlEqualTo("/api/"))
                .inScenario("Check data of student")
                .whenScenarioStateIs(Scenario.STARTED)
                .willSetStateTo("Step 1")
                .willReturn(
                    WireMock.ok(fileToString("mock/user_igor.json"))
                )
        )

        run {
            FavouritePersonsPage {
                step("Add Students") {
                    clickAddPersonButton()
                    clickAddPersonByNetworkButton()
                    Thread.sleep(3000)
                    clickPerson()
                }
            }
            EditPage {
                step("Replace students name") {
                    clearName()
                    enterName("Иосиф")
                    clickSaveButton()
                }
            }
            FavouritePersonsPage {
                step("Check students name") {
                    checkName()
                }
            }
        }
    }

    @Test
    fun addStudentByManually() {
        run {
            FavouritePersonsPage {
                step("Jump to add student") {
                    clickAddPersonButton()
                    clickAddPersonManuallyButton()
                }
            }
            EditPage {
                step("Enter info about student") {
                    enterName("Ryan")
                    enterSurname("Gosling")
                    enterGender("M")
                    enterBirthday("2000-01-01")
                    enterEmail("r.gos@gmail.com")
                    enterPhone("8-383-876-5463")
                    enterAddress("Ya, California, R Street - 87, USA")
                    enterImage("https://encrypted-tbn2.gstatic.com/licensed-image?q=tbn:ANd9GcQu1ctuIA3vJZ3UPy7w46jqaaVt2Kxa01Wz68_gtrauNeDHyNcBuFqCfFaFiORUl78sK8FOxwDDQJ7l-jQ")
                    enterScore("100")
                    clickSaveButton()
                }
            }
            FavouritePersonsPage {
                step("Check info about student") {
                    checkNewStudentsData("Ryan Gosling", "r.gos@gmail.com", "8-383-876-5463", "Ya, California, R Street - 87, USA", "100", "Male, 24")

                }
            }
        }
    }

    @Test
    fun checkError() {
        run {
            FavouritePersonsPage {
                step("Click add person") {
                    clickAddPersonButton()
                    clickAddPersonManuallyButton()
                }
            }
            EditPage {
                step("Check error for gender") {
                    clickSaveButton()
                    checkErrorForGender()
                }
            }
        }
    }

    @Test
    fun checkNoErrorForGenderField() {
        run {
            FavouritePersonsPage {
                step("Click add student") {
                    clickAddPersonButton()
                    clickAddPersonManuallyButton()
                }
            }
            EditPage {
                step("Check error doesnt exist") {
                    enterGender("M")
                    clickSaveButton()
                    checkNoErrorForGender()
                }
            }
        }
    }
}