package ru.tinkoff.favouritepersons

import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.ok
import com.github.tomakehurst.wiremock.client.WireMock.stubFor
import com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.github.tomakehurst.wiremock.stubbing.Scenario.STARTED
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import fileToString
import kotlinx.serialization.json.Json
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.rules.RuleChain
import ru.tinkoff.favouritepersons.data.Person
import ru.tinkoff.favouritepersons.pages.FavouritePersonsPage
import ru.tinkoff.favouritepersons.presentation.activities.MainActivity
import ru.tinkoff.favouritepersons.room.PersonDataBase
import ru.tinkoff.myupgradeapplication.week6.rules.LocalhostPreferenceRule

@RunWith(AndroidJUnit4::class)
class ActionsWithStudentsTest : TestCase() {
    @get:Rule
    val ruleChain: RuleChain = RuleChain.outerRule(LocalhostPreferenceRule())
        .around(WireMockRule(5000))
        .around(ActivityScenarioRule(MainActivity::class.java))

    private lateinit var db: PersonDataBase
    private val person = Json.decodeFromString<Person>(fileToString("mock/user_igor.json"))

    @Before
    fun createDb() {
        db = PersonDataBase.getDBClient(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @After
    fun clearDB() {
        db.personsDao().clearTable()
    }

    @Test
    fun addStudentTest() {
        stubFor(
            get(urlEqualTo("/api/"))
                .willReturn(
                    ok(fileToString("mock/user_igor.json"))
                )
        )

        run {
            FavouritePersonsPage {
                step("Add Student") {
                    clickAddPersonButton()
                    clickAddPersonByNetworkButton()
                    Thread.sleep(1000)
                    checkMessageStudentsAbsent()
                }
            }
        }
    }

    @Test
    fun deleteStudentTest() {
        var countOfCard: Int = 0

        stubFor(
            get(urlEqualTo("/api/"))
                .willReturn(
                    ok(fileToString("mock/user_igor.json"))
                )
        )

        run {
            FavouritePersonsPage {
                step("Add three students") {
                    clickAddPersonButton()
                    for (i in 0..2) {
                        clickAddPersonByNetworkButton()
                    }
                }
                step("Delete student") {
                    deleteStudent()
                }
                step("Check student deletion") {
                    countOfCard = rvNotes.getSize()
                    checkStudentDeletion(countOfCard)
                }
            }
        }
    }

    @Test
    fun defaultValueInBottomsheetTest() {
        run {
            FavouritePersonsPage {
                step("Open bottomsheet") {
                    bottomsheetIsVisible()
                }
                step("Check defaulte value") {
                    checkDefaulteValueInBottomsheet()
                }
            }
        }
    }

    @Test
    fun checkSortDefaultValue() {
        stubFor(
            get(urlEqualTo("/api/"))
                .inScenario("Check sort by age")
                .whenScenarioStateIs(STARTED)
                .willSetStateTo("Step 1")
                .willReturn(
                    ok(fileToString("mock/user_igor.json"))
                )
        )

        stubFor(
            get(urlEqualTo("/api/"))
                .inScenario("Check sort by age")
                .whenScenarioStateIs("Step 1")
                .willSetStateTo("Step 2")
                .willReturn(
                    ok(fileToString("mock/user_sergey.json"))
                )
        )

        stubFor(
            get(urlEqualTo("/api/"))
                .inScenario("Check sort by age")
                .whenScenarioStateIs("Step 2")
                .willSetStateTo("Final")
                .willReturn(
                    ok(fileToString("mock/user_danil.json"))
                )
        )

        run {
            FavouritePersonsPage {
                step("Check default value for sort") {
                    clickAddPersonButton()
                    for (i in 0..2) {
                        clickAddPersonByNetworkButton()
                    }
                    bottomsheetIsVisible()
                    clickSortByAge()
                    checkSortByAge()
                }
            }
        }
    }


}