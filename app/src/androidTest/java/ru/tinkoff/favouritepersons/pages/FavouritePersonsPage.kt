package ru.tinkoff.favouritepersons.pages

import android.provider.ContactsContract.CommonDataKinds.Email
import android.provider.ContactsContract.CommonDataKinds.Phone
import android.util.Log
import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import fileToString
import io.github.kakaocup.kakao.check.KCheckBox
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KSnackbar
import io.github.kakaocup.kakao.text.KTextView
import junit.framework.Assert.assertEquals
import kotlinx.serialization.json.Json
import org.hamcrest.Matcher
import ru.tinkoff.favouritepersons.R
import ru.tinkoff.favouritepersons.data.Person

class FavouritePersonsPage : BaseScreen() {
    private val addPersonButton = KButton { withId(R.id.fab_add_person) }
    private val addPersonByNetworkButton = KButton { withId(R.id.fab_add_person_by_network) }
    private val addPersonManuallyButton = KButton { withId(R.id.fab_add_person_manually) }
    private val messageStudentsAbsent = KTextView { withId(R.id.tw_no_persons) }
    private val botomsheet = KView { withId(R.id.action_item_sort) }
    private val actionItemSortButton = KButton { withId(R.id.bsd_rb_age) }
    private val person = Json.decodeFromString<Person>(fileToString("mock/user_igor.json"))
    private val defaultValueInBottomsheet = KCheckBox { withId(R.id.bsd_rb_default) }
    //private val errorConnection =  { withId(com.google.android.material.R.id.snackbar_text) }

    fun clickAddPersonButton() {
        addPersonButton.click()
    }

    fun clickAddPersonByNetworkButton() {
        addPersonByNetworkButton.click()
    }

    fun clickAddPersonManuallyButton() {
        addPersonManuallyButton.click()
    }

    fun checkMessageStudentsAbsent() {
        messageStudentsAbsent.isNotDisplayed()
    }

    fun checkStudentDeletion(countOfStudents: Int) {
        assertEquals("", 2, countOfStudents)
    }

    fun deleteStudent() {
        rvNotes {
            childAt<FavouritePersonsPage.NoteItemScreen>(0) {
                view.perform(ViewActions.swipeLeft())
            }
        }
    }

    fun bottomsheetIsVisible() {
        botomsheet.click()
    }

    fun clickSortByAge() {
        actionItemSortButton.click()
    }

    fun checkDefaulteValueInBottomsheet() {
        defaultValueInBottomsheet.isChecked()
    }

    fun clickPerson() {
        rvNotes {
            childAt<FavouritePersonsPage.NoteItemScreen>(0) {
                userAge.click()
            }
        }
    }

    fun checkName() {
        rvNotes {
            childAt<FavouritePersonsPage.NoteItemScreen>(0) {
                userName.hasText("Иосиф Ivanov")
            }
        }
    }

    fun checkSortByAge() {
        rvNotes {
            childAt<FavouritePersonsPage.NoteItemScreen>(0) {
                userAge.hasText("Male, 18")
            }
            childAt<FavouritePersonsPage.NoteItemScreen>(1) {
                userAge.hasText("Male, 20")
            }
            childAt<FavouritePersonsPage.NoteItemScreen>(2) {
                userAge.hasText("Male, 21")
            }
        }
    }

    fun checkErrorConnection() {
        onView(withId(com.google.android.material.R.id.snackbar_text)).check(matches(withText("Internet error! Check your connection")))
    }

    fun checkNewStudentsData(
        name: String,
        email: String,
        phone: String,
        address: String,
        score: String,
        ageAndGender: String
    ) {
        rvNotes {
            childAt<FavouritePersonsPage.NoteItemScreen>(0) {
                userName.hasText(name)
                userAge.hasText(ageAndGender)
                userEmail.hasText(email)
                userPhone.hasText(phone)
                userAddress.hasText(address)
                userScore.hasText(score)
            }
        }
    }

    val rvNotes = KRecyclerView(
        builder = { withId(R.id.rv_person_list) },
        itemTypeBuilder = { itemType(::NoteItemScreen) }
    )

    class NoteItemScreen(matcher: Matcher<View>) : KRecyclerItem<NoteItemScreen>(matcher) {
        val userAge = KTextView(matcher) { withId(R.id.person_private_info) }
        val userCard = KView(matcher) { ViewMatchers.withId(R.id.cl_personInfo) }
        val userName = KTextView(matcher) { withId(R.id.person_name) }
        val userEmail = KTextView(matcher) { withId(R.id.person_email) }
        val userPhone = KTextView(matcher) { withId(R.id.person_phone) }
        val userAddress = KTextView(matcher) { withId(R.id.person_address) }
        val userScore = KTextView(matcher) { withId(R.id.person_rating) }
    }

    companion object {
        inline operator fun invoke(crossinline block: FavouritePersonsPage.() -> Unit) =
            FavouritePersonsPage().block()
    }
}