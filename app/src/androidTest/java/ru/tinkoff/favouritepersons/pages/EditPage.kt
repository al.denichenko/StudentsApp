package ru.tinkoff.favouritepersons.pages

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.hasErrorText
import androidx.test.espresso.matcher.ViewMatchers.withId
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import junit.framework.Assert.assertEquals
import org.hamcrest.Matcher
import ru.tinkoff.favouritepersons.R
import ru.tinkoff.favouritepersons.presentation.PersonErrorMessages

class EditPage {
    private val name = KEditText { withId(R.id.et_name) }
    private val surname = KEditText { withId(R.id.et_surname) }
    private val gender = KEditText { withId(R.id.et_gender) }
    private val birthday = KEditText { withId(R.id.et_birthdate) }
    private val saveButton = KButton { withId(R.id.submit_button) }
    private val email = KEditText { withId(R.id.et_email) }
    private val phone = KEditText { withId(R.id.et_phone) }
    private val address = KEditText { withId(R.id.et_address) }
    private val image = KEditText { withId(R.id.et_image) }
    private val score = KEditText { withId(R.id.et_score) }
    private val errorGender = KEditText { withId(R.id.til_gender) }

    fun checkName() {
        name.hasText("Igor")
    }

    fun checkSurname() {
        surname.hasText("Ivanov")
    }

    fun checkGender() {
        gender.hasText("М")
    }

    fun checkBirthday() {
        birthday.hasText("2000-01-01")
    }

    fun clearName() {
        name.clearText()
    }

    fun enterName(newName: String) {
        name.perform { replaceText(newName) }
    }

    fun enterSurname(newSurname: String) {
        surname.perform { replaceText(newSurname) }
    }

    fun enterGender(newGender: String) {
        gender.perform { replaceText(newGender) }
    }

    fun enterBirthday(newBirthday: String) {
        birthday.perform { replaceText(newBirthday) }
    }

    fun enterEmail(newEmail: String) {
        email.perform { replaceText(newEmail) }
    }

    fun enterPhone(newPhone: String) {
        phone.perform { replaceText(newPhone) }
    }

    fun enterAddress(newAddress: String) {
        address.perform { replaceText(newAddress) }
    }

    fun enterImage(newImage: String) {
        image.perform { replaceText(newImage) }
    }

    fun enterScore(newScore: String) {
        score.perform { replaceText(newScore) }
    }

    fun clickSaveButton() {
        saveButton.click()
    }

    fun checkErrorForGender() {
        errorGender.hasText("Поле должно быть заполнено буквами М или Ж")
    }

    fun checkNoErrorForGender() {
        errorGender.hasNoText("Поле должно быть заполнено буквами М или Ж")
    }

    companion object {
        inline operator fun invoke(crossinline block: EditPage.() -> Unit) =
            EditPage().block()
    }
}